# Hummingbird for Linux and macOS

#### AirVPN's free and open source OpenVPN 3 client based on AirVPN's OpenVPN 3 library fork

### Version 1.3.0 - Release date 1 June 2023


## Note on Checksum Files

Hummingbird is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of hummingbird which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" hummingbird
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of Hummingbird to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the hummingbird client. Please note the files contained in the
distribution tarballs are created from the very source code available in the
master branch of the [official hummingbird's repository](https://gitlab.com/AirVPN/hummingbird).


### Checksum codes for Version 1.3.0

The checksum codes contained in files `hummingbird-<os>-<arch>-1.3.0.<archive>.sha512`
and `hummingbird.sha512` must correspond to the codes below in order to prove
they are genuinely created and distributed by AirVPN.

***Please note hummingbird for Linux is distributed with AirVPN Suite***


## macOS ARM64 plain

`hummingbird-macos-arm64-1.3.0.tar.gz`:
1aa405e3257599d6dca4bea5920ba2fe7e0e1d4a99525aa668d03ebb69d65abb79a754cf072b2497a57479c409a8b01c214e3d79dfaf52b45593a40bad112811

`hummingbird`:
30ce7709dca6fcc66697842cd799edebd332f9f4f2aaa49550e9b99c93cebd00fd01ed19efbff88624e0e12fae06a24333f8e867f23a742bb788af5c06f26807


## macOS ARM64 notarized

`hummingbird-macos-arm64-notarized-1.3.0.zip`:
c77091a9d1c6e590d3fdecebdc5d8ef27cc12512e60919a7d63d6bd98d0c4c1f04ba63c31cdba06e95ae5947f35f5b8756b488867ab7540e272c8f7be5265cf2

`hummingbird`:
b33c5858878a49f640f4146c77e3f8d88c3ea0a085d672b0181784a13978a6ecf3d7edf95ff17515aee4cc7028333a7adfbb0adbbcd266724c0ac764b149ba1d


## macOS x86_64 plain

`hummingbird-macos-x86_64-1.3.0.tar.gz`:
8218e49c5be623fce9bced4f818f39e5db99bfe32b011927811fd093487c2549c6142bcfd827e9fa87d7ff22fe4aff935b3433365bcba7d29ddc2005a8504bb0

`hummingbird`:
d1094f14237d88a2063ccfddd9498c22efe8f126dcbb67694b8945472f363caab32711364072839d745d5b1e46c6f2cb26866d5a2bb4cbafe4d40e00281d89e1


## macOS x86_64 notarized

`hummingbird-macos-x86_64-notarized-1.3.0.zip`:
d5013827cb323a9a2a53f0890c3cabf074f74ecb1ccb045137384a2df026425f42a27a17ef114d2bf35cc23d6598e7757f30bb345727a8c5bb967507838bae0b

`hummingbird`:
646396c93817b4ff3cbd2c3e84cf66df25d1163914dfd37f77b790c7f6d596e6000ac60e40c0a7a6443f9df267c1e51dffc3e986ae6b63bbf54b510e816f0f13
