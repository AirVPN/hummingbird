/*
 * localnetwork.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <string>

enum class IPFamily
{
    IPv4,
    IPv6,
    ANY
};
 
typedef struct IPAddress
{
    std::string address = "";
    IPFamily family = IPFamily::IPv4;
    int prefixLength = 0;
} IPAddress;

bool operator==(const IPAddress &lval, const IPAddress &rval);
bool operator!=(const IPAddress &lval, const IPAddress &rval);

class LocalNetwork
{
    public:

    LocalNetwork();
    ~LocalNetwork();

#if defined(__linux__) && !defined(__ANDROID__)

    typedef struct Gateway
    {
        std::string address = "";
        std::string interface = "";
    } Gateway;

#endif 

    bool isIPv6Enabled();
    std::string getLoopbackInterface();
    std::vector<IPAddress> getLocalIPaddresses();
    std::vector<std::string> getLocalInterfaces();
    static IPAddress parseIpSpecification(std::string ipSpec);

#if defined(__linux__) && !defined(__ANDROID__)

    Gateway getDefaultGatewayInterface();

#endif

    protected:

    void scanIpAddresses();
    void scanInterfaces();

    std::vector<IPAddress> localIPaddress;
    std::vector<std::string> localInterface;
    std::string loopbackInterface;
};

