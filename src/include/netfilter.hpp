/*
 * netfilter.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2022 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <sstream>
#include <vector>
#include "airvpntools.hpp"
#include "localnetwork.hpp"

extern void global_log(const std::string &s);
extern void global_syslog(const std::string &s);

class NetFilter
{
    public:

    enum class Mode
    {
        OFF,
        AUTO,
        NFTABLES,
        IPTABLES,
        PF,
        UNKNOWN
    };

    enum class Protocol
    {
        UDP,
        TCP,
        ANY
    };

    enum class Hook
    {
        INPUT,
        OUTPUT,
        PREROUTING,
        POSTROUTING,
        FORWARD
    };

    enum class Policy
    {
        DROP,
        REJECT,
        ACCEPT,
        NONE
    };

    enum class Validity
    {
        PERMANENT,
        SESSION
    };

    enum class Action
    {
        ADD,
        INSERT,
        DELETE
    };

    const static int ITEM_NOT_FOUND = -2;
    const static int ITEM_ERROR = -1;

    class Item
    {
        public:

        static constexpr char FAMILY_IPV4[] = "ip";
        static constexpr char FAMILY_IPV6[] = "ip6";
        static constexpr char FAMILY_INET[] = "inet";
        static constexpr char FAMILY_ARP[] = "arp";
        static constexpr char FAMILY_BRIDGE[] = "bridge";
        static constexpr char FAMILY_NETDEV[] = "netdev";

        static const int TARGET_ALL = 1;
        static const int TARGET_NFTABLES = (1 << 1);
        static const int TARGET_IPTABLES = (1 << 2);
        static const int TARGET_PF = (1 << 3);

        enum Type
        {
            TABLE,
            CHAIN,
            RULE
        };

        Item(Type type, Validity v=Validity::PERMANENT);
        ~Item();

        Type getItemType() const;
        std::string getFamily() const;
        std::string getName() const;
        Validity getValidity();
        unsigned long getHandle();
        void setHandle(unsigned long h);
        int getTarget();
        bool isTargetAll();
        bool isTargetNFTables();
        bool isTargetIPTables();
        bool isTargetPF();

        private:
            
        Type itemType;
        Validity validity;
        int target;
        std::string family;
        std::string name;
        unsigned long handle;

        protected:

        void setFamily(const std::string &f);
        void setName(const std::string &n);
        void setValidity(Validity v);
        void setTarget(int t);
        void setSessionName(const std::string &s);
    };

    class Table : public Item
    {
        public:

        static constexpr char MANGLE[] = "mangle";
        static constexpr char NAT[] = "nat";
        static constexpr char FILTER[] = "filter";

        Table(const std::string &f, const std::string &n, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT);
        ~Table();

        Table *clone() const { return new Table(*this); }

        void setDormant(bool d);
        bool isDormant();

        private:
            
        bool dormant;
    };

    class Chain : public Item
    {
        public:
            
        static constexpr char NONE[] = "NONE";
        static constexpr char INPUT[] = "INPUT";
        static constexpr char OUTPUT[] = "OUTPUT";
        static constexpr char PREROUTING[] = "PREROUTING";
        static constexpr char POSTROUTING[] = "POSTROUTING";
        static constexpr char FORWARD[] = "FORWARD";

        static constexpr char TYPE_FILTER[] = "filter";
        static constexpr char TYPE_NAT[] = "nat";
        static constexpr char TYPE_ROUTE[] = "route";

        static const int PRIORITY_RAW = -300;
        static const int PRIORITY_MANGLE = -150;
        static const int PRIORITY_DSTNAT = -100;
        static const int PRIORITY_FILTER = 0;
        static const int PRIORITY_SECURITY = 50;
        static const int PRIORITY_SRCNAT = 100;

        Chain(const std::string &f, const std::string &t, const std::string &n, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT);
        Chain(Table &t, const std::string &n, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT);
        Chain(Table *t, const std::string &n, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT);
        ~Chain();

        Chain *clone() const { return new Chain(*this); }

        std::string getTable() const;
        void setType(const std::string &t);
        std::string getType();
        void setHook(Hook h);
        Hook getHook();
        void setDevice(const std::string &d);
        std::string getDevice();
        void setPriority(int p);
        int getPriority();
        void setPolicy(Policy p);
        Policy getPolicy();
        
        private:

        std::string table;
        std::string type;
        Hook hook;
        std::string device;
        int priority;
        Policy policy;
        
        void init();
    };

    class Rule : public Item
    {
        public:

        Rule(const std::string &f, const std::string &t, const std::string &c, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT, const std::string &n = "");
        Rule(Chain &c, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT, const std::string &n = "");
        Rule(Chain *c, int tg=Item::TARGET_ALL, Validity v=Validity::PERMANENT, const std::string &n = "");
        ~Rule();

        Rule *clone() const { return new Rule(*this); }

        std::string getTable() const;
        std::string getChain() const;

        void setProtocol(Protocol p);
        Protocol getProtocol() const;
        void setSourceInterface(const std::string &i);
        std::string getSourceInterface() const;
        void setSourceAddress(IPAddress ip);
        IPAddress getSourceAddress() const;
        void setSourcePort(int p);
        int getSourcePort() const;
        void setDestinationInterface(const std::string &i);
        std::string getDestinationInterface() const;
        void setDestinationAddress(IPAddress ip);
        IPAddress getDestinationAddress() const;
        void setDestinationPort(int p);
        int getDestinationPort() const;
        void setPolicy(Policy p);
        Policy getPolicy() const;
        void setCounter(bool d);
        bool isCounterSet() const;
        void setStatement(const std::string &s);
        std::string getStatement() const;

        private:

        std::string table;
        std::string chain;

        Protocol protocol;
        std::string sourceInterface;
        IPAddress sourceAddress;
        int sourcePort;
        std::string destinationInterface;
        IPAddress destinationAddress;
        int destinationPort;
        Policy policy;
        bool counter;
        std::string statement;
        
        void init();
    };

    NetFilter(const std::string &workdir, Mode mode = Mode::AUTO);
    ~NetFilter();

    bool systemBackupExists();
    bool init();
    bool restore();
    void setup(const std::string &loopbackIface);
    bool commitRules();
    bool addAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool addRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRemoveAllowRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    bool commitRemoveRejectRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    Mode getMode();
    bool setMode(Mode mode);
    std::string getModeDescription();
    std::string getModeDescription(Mode mode);
    bool addIgnoredInterface(const std::string &interface);
    void clearIgnoredInterfaces();
    bool isFirewalldRunning();
    bool isUfwRunning();
    bool isIPTablesLegacy();
    bool isNetworkLockAvailable();
    bool isNetworkLockEnabled();

    bool isIptableFilterAvailable();
    bool isIptableNatAvailable();
    bool isIptableMangleAvailable();
    bool isIptableSecurityAvailable();
    bool isIptableRawAvailable();

    bool isIp6tableFilterAvailable();
    bool isIp6tableNatAvailable();
    bool isIp6tableMangleAvailable();
    bool isIp6tableSecurityAvailable();
    bool isIp6tableRawAvailable();

    int insert(Item &item);
    int append(Item &item);
    int remove(Item &item);
    int commit(Item &item);
    int search(Item &item);
    bool rollbackSession();

    Rule *createSessionFilterRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    Rule *createEmptySessionFilterRule(IPFamily ipFamily, Hook hook);

    private:

    std::vector<Item *> itemList;

    const char *systemctlBinary = "systemctl";
    const char *shellBinary = "sh";

    AirVPNTools::InitSystemType initSystemType = AirVPNTools::InitSystemType::Unknown;

    char binpath[128];

    char *charBuffer = NULL;

    const int charBufferSize = (1024 * 1024) * 5;  // 5Mb

    Mode filterMode = Mode::UNKNOWN;
    std::string workingDirectory = "";
    std::string loopbackInterface = "";
    std::vector<std::string> ignoredInterface;

    LocalNetwork *localNetwork = nullptr;

    bool firewalldAvailable = false;
    bool ufwAvailable = false;
    bool nftablesAvailable = false;
    bool iptablesAvailable = false;
    bool iptablesLegacy = false;
    bool pfAvailable = false;
    bool pfNeedsFullFlush = true;
    bool networkLockEnabled = false;

    bool checkService(const std::string &name);
    bool readFile(const std::string &fname, char *buffer, int size);

    void initItemList(const std::string &loopbackIface);
    int addItem(Item *i, int p=-1);
    std::string translateItem(Item *item, Action action);
    std::string translateItem(Item *item, int target, Action action);
    std::string translateItemToNFTables(Item *item, Action action);
    std::string translateItemToIPTables(Item *item, Action action);
    std::string translateItemToPF(Item *item, Action action);
    void checkFilterTableChain(IPFamily ipFamily, Hook hook);

    // nftables

    const char *nftBinary = "nft";

    std::string nftablesSaveFile = "nftables-save.txt";
    std::string nftablesNetFilterRulesFile = "nftables-netfilter-rules.txt";
    std::string nftablesRules = "";

    bool nftablesSave();
    bool nftablesRestore();
    bool nftablesFlush();
    void nftablesSetup(const std::string &loopbackIface);
    void nftablesResetRules();
    std::string createNftablesGenericRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void nftablesAddRule(const std::string &rule);
    bool nftablesCommitRule(const std::string &rule);
    bool nftablesCommitItem(Item *item, Action action=Action::ADD);
    bool nftablesCommit();

    // iptables

    const char *iptablesBinary = NULL;
    const char *iptablesSaveBinary = NULL;
    const char *iptablesRestoreBinary = NULL;
    const char *iptablesCurrentBinary = "iptables";
    const char *iptablesCurrentSaveBinary = "iptables-save";
    const char *iptablesCurrentRestoreBinary = "iptables-restore";
    const char *iptablesLegacyBinary = "iptables-legacy";
    const char *iptablesLegacySaveBinary = "iptables-legacy-save";
    const char *iptablesLegacyRestoreBinary = "iptables-legacy-restore";
    const char *ip6tablesBinary = NULL;
    const char *ip6tablesSaveBinary = NULL;
    const char *ip6tablesRestoreBinary = NULL;
    const char *ip6tablesCurrentBinary = "ip6tables";
    const char *ip6tablesCurrentSaveBinary = "ip6tables-save";
    const char *ip6tablesCurrentRestoreBinary = "ip6tables-restore";
    const char *ip6tablesLegacyBinary = "ip6tables-legacy";
    const char *ip6tablesLegacySaveBinary = "ip6tables-legacy-save";
    const char *ip6tablesLegacyRestoreBinary = "ip6tables-legacy-restore";

    std::string iptablesSaveFile = "iptables-save.txt";
    std::string ip6tablesSaveFile = "ip6tables-save.txt";
    std::string iptablesRules = "";
    std::string ip6tablesRules = "";

    bool iptableFilterAvailable;
    bool iptableNatAvailable;
    bool iptableMangleAvailable;
    bool iptableSecurityAvailable;
    bool iptableRawAvailable;

    bool ip6tableFilterAvailable;
    bool ip6tableNatAvailable;
    bool ip6tableMangleAvailable;
    bool ip6tableSecurityAvailable;
    bool ip6tableRawAvailable;

    bool iptablesSave(IPFamily ipFamily);
    bool iptablesRestore(IPFamily ipFamily);
    bool iptablesFlush(IPFamily ipFamily);
    void iptablesSetup(const std::string &loopbackIface);
    void iptablesResetRules(IPFamily ipFamily);
    std::string createIptablesGenericRule(Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void iptablesAddRule(IPFamily ipFamily, const std::string &rule);
    bool iptablesCommitRule(IPFamily ipFamily, const std::string &rule);
    bool iptablesCommitItem(Item *item, Action action=Action::ADD);
    bool iptablesCommit(IPFamily ipFamily);

    // pf

    const char *pfctlBinary = "pfctl";

    std::string pfSaveFile = "pf-save.txt";
    std::string pfNetFilterRulesFile = "pf-netfilter-rules.txt";
    std::string pfConfFile = "/etc/pf.conf";
    std::string pfRules = "";

    bool pfEnable();
    bool pfSave();
    bool pfRestore();
    bool pfFlush();
    bool pfFlushAll();
    bool pfFlushRules();
    void pfSetup(const std::string &loopbackIface);
    void pfResetRules();
    std::string createPfGenericRule(IPFamily ipFamily, Hook hook, const std::string &interface, Protocol protocol, const std::string &sourceIP, int sourcePort, const std::string &destinationIP, int destinationPort);
    void pfAddRule(const std::string &rule);
    bool pfCommit();
    bool pfAddIgnoredInterfaces();
};

bool operator==(const NetFilter::Table &lval, const NetFilter::Table &rval);
bool operator!=(const NetFilter::Table &lval, const NetFilter::Table &rval);

bool operator==(const NetFilter::Chain &lval, const NetFilter::Chain &rval);
bool operator!=(const NetFilter::Chain &lval, const NetFilter::Chain &rval);

bool operator==(const NetFilter::Rule &lval, const NetFilter::Rule &rval);
bool operator!=(const NetFilter::Rule &lval, const NetFilter::Rule &rval);

class NetFilterException : public std::exception
{
    public:

    explicit NetFilterException(const char* m)
    {
        message = m;
    }

    explicit NetFilterException(const std::string &m)
    {
        message = m;
    }

    virtual ~NetFilterException() throw()
    {
    }

    virtual const char *what() const throw()
    {
       return message.c_str();
    }

    protected:

    std::string message;
};
